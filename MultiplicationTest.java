import org.junit.Test;

import static org.junit.Assert.*;

public class MultiplicationTest {

    @Test
    public void getResult() {

        int a,b,c;

        a = (int)(Math.random()*100);
        b = (int)(Math.random()*100);
        c = a * b;

        assertEquals(Multiplication.getResult(a,b),c);
    }
        @Test
    public void zero() {

        int a,b,c;

        a = 0;
        b = -100 +(int)(Math.random()*100);
        c = a * b;

        assertEquals(Multiplication.getResult(a,b),c);
    }
    @Test
    public void zeroTwo() {

        int a,b,c;

        a = -100 +(int)(Math.random()*100);
        b = 0;
        c = a * b;

        assertEquals(Multiplication.getResult(a,b),c);
    }
 
    @Test
    public void zeroThree() {

        int a,b,c;

        a = 0;
        b = 0;
        c = a * b;

        assertEquals(Multiplication.getResult(a,b),c);
    }
}
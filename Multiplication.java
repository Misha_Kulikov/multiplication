import java.util.Scanner;

public class Multiplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int result = 0;
        System.out.print("Введите первый множитель: ");
        int factorOne = scanner.nextInt();

        System.out.print("Введите второй множитель: ");
        int factorTwo = scanner.nextInt();

        while (factorOne != 0 && factorTwo!=0) {
            if (factorOne > 0 && factorTwo > 0) {
                result += factorTwo;
                factorOne--;
            } else {
                result -= factorTwo;
                factorOne++;
            }
        }
        System.out.print("Результат умножения равен: " + result);
    }
}